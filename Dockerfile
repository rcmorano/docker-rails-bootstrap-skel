FROM rcmorano/saucy-rvm
RUN apt-get update
RUN apt-get install --assume-yes --force-yes vim git
RUN /bin/bash --login -c 'rvm gemset create docker-rails-bootstrap-skel'
RUN /bin/bash --login -c 'rvm gemset use docker-rails-bootstrap-skel; gem install --no-ri --no-rdoc bundle rails'
# install project gem depends
RUN /bin/bash --login -c 'git clone https://bitbucket.org/rcmorano/docker-rails-bootstrap-skel.git /docker-rails-bootstrap-skel'

ENTRYPOINT ["/docker-rails-bootstrap-skel/bin/create-rails-app"]
